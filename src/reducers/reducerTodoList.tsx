import todoListItem from "../Components/todoListItem";
import {TodoListActions} from "../actions/action-types";


const todos = (state: todoListItem[] = [], action: TodoListActions): todoListItem[] => {
    switch (action.type) {
        case 'ADD_TASK':
            return [...state,action.task];
        case 'TOGGLE_COMPLETE':
            return  state.map(task => {
                if(task.id === action.id){
                    return{
                        ...task,
                        isCompleted: !task.isCompleted
                    }
                } else {
                    return task;
                }
            });
        case 'DELETE_TASK':
            return state.filter(task => task.id !== action.id);
        default:
            return state;
    }
};

export default todos;
