import {combineReducers} from "redux";
import todoList from "./reducerTodoList";
import todoListItem from "../Components/todoListItem";

export type State = {
    todoList: todoListItem[]
}

const allReducers =  combineReducers<State>({
    todoList
});

export default allReducers;

