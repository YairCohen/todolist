import React, {ChangeEvent, FormEvent, useState} from "react";
import todoListItem from "./todoListItem";
import {useDispatch} from "react-redux";
import {addTask} from "../actions";

interface PropsData {
    todoList: todoListItem[]
}

function AddNewTask(props: PropsData) {
    const [newTask, setNewTask] = useState<string>("");
    const dispatch = useDispatch();

    function handleChange(event: ChangeEvent<HTMLInputElement>) {
        setNewTask(event.target.value)
    }

    function getNewId() {
        if(props.todoList.length === 0){
            return 1;
        } else {
            return props.todoList[props.todoList.length -1].id + 1
        }
    }

    function isHaveSameTask() {
        return props.todoList.find(task => task.taskName === newTask);
    }

    function handleSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault();
        if(newTask === ''){
            alert('אופס..  שכחת לכתוב משימה!!')
        } else if(isHaveSameTask()) {
            alert('אופס..  יש כבר משימה כזאת!!')
        } else {
            dispatch(addTask({taskName: newTask, isCompleted: false, id: getNewId()}));
            setNewTask("");
        }
    }


    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                value={newTask}
                onChange={handleChange}
                placeholder={'הכנס משימה חדשה'}
            />
        </form>
    );
}
export default AddNewTask;