import React, {ChangeEvent} from 'react';

interface PropsData {
    search: (event: ChangeEvent<HTMLInputElement>) => void,
}

function Search(props: PropsData) {
    return (
        <input type="text" className="input" onChange={props.search} placeholder="חיפוש" />
    );
}

export default Search;
