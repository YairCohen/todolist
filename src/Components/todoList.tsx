import React from 'react';
import todoListItem from "./todoListItem";
import Task from "./task";

interface PropsData {
    todoListAfterSearch: todoListItem[],
}

function TodoList(props:PropsData) {

    return (
        <div>
            {props.todoListAfterSearch.map((task) => (
                <Task key={task.id} task={task}/>
            ))}
        </div>
    );
}

export default TodoList;
