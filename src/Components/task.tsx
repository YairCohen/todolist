import React from 'react';
import todoListItem from "./todoListItem";
import {useDispatch} from "react-redux";
import {deleteTask, toggleComplete} from "../actions";

interface PropsData {
    task: todoListItem
}

function Task(props: PropsData) {
    const dispatch = useDispatch();

    return (
        <div className= 'task'>
            <div className= {props.task.isCompleted ? 'completeTask' : 'notCompleteTask'} onClick={() => dispatch(toggleComplete(props.task.id))}>{props.task.taskName}</div>
            <button onClick={() => dispatch(deleteTask(props.task.id))}>x</button>
        </div>
    );
}

export default Task;
