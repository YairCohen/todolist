type todoListItem = {
    taskName: string,
    isCompleted: boolean,
    id: number
}

export default todoListItem;
