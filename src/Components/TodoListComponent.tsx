import React, {ChangeEvent, useEffect, useState} from 'react';
import AddNewTask from "./AddNewTask";
import TodoList from "./todoList";
import todoListItem from "./todoListItem";
import "./TodoListComponent.css";
import Search from "./search";
import {useSelector} from "react-redux";
import {State} from "../reducers";

function TodoListComponent() {
    const todoList: todoListItem[] = useSelector((state: State) => state.todoList);
    const [wordForSearch,setWordForSearch] = useState('');
    const todoListAfterSearch = useFilteredList(todoList, wordForSearch);

    function useFilteredList(todoList: todoListItem[], wordForSearch: string) {
        const [todoListAfterSearch, settodoListAfterSearch] = useState<todoListItem[]>([]);

        useEffect(() => {
            let newList = [];

            if (wordForSearch !== "") {
                const wordForSearchInLowerCase = wordForSearch.toLowerCase()
                newList = todoList.filter(item => {
                    return item.taskName.toLowerCase().includes(wordForSearchInLowerCase);
                });
            } else {
                newList = todoList;
            }
            settodoListAfterSearch(newList)
        }, [todoList,wordForSearch]);

        return todoListAfterSearch;
    }

    function search(event: ChangeEvent<HTMLInputElement>) {
            setWordForSearch(event.target.value);
    }

    return (
        <div className='TodoList'>
            <h1>רשימת משימות</h1>
            <Search search={search} />
            <TodoList todoListAfterSearch={todoListAfterSearch}/>
            <AddNewTask todoList={todoList}/>
        </div>
    );
}

export default TodoListComponent;
