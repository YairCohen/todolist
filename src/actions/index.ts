import {TodoListActions} from "./action-types";
import todoListItem from "../Components/todoListItem";

export const addTask = (task: todoListItem): TodoListActions => ({
    type: 'ADD_TASK',
    task: task
});

export const toggleComplete = (id: number): TodoListActions => ({
    type: 'TOGGLE_COMPLETE',
    id: id
});

export const deleteTask = (id: number): TodoListActions => ({
    type: 'DELETE_TASK',
    id: id
});
