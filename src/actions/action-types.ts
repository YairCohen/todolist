import todoListItem from "../Components/todoListItem";

export const ADD_TASK = 'ADD_TASK';
export const TOGGLE_COMPLETE = 'TOGGLE_COMPLETE';
export const DELETE_TASK = 'DELETE_TASK';


interface AddTaskAction {
    type: typeof ADD_TASK
    task: todoListItem
}

interface ToggleCompleteAction {
    type: typeof TOGGLE_COMPLETE
    id: number
}

interface DeleteTaskAction {
    type: typeof DELETE_TASK
    id: number
}

export type TodoListActions = AddTaskAction | ToggleCompleteAction | DeleteTaskAction;
