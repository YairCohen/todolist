import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TodoListComponent from './Components/TodoListComponent';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import allReducers from './reducers'

const store = createStore(allReducers);

ReactDOM.render(

    <Provider store={store}>
        <TodoListComponent />
    </Provider>
    , document.getElementById('root'));

serviceWorker.unregister();
